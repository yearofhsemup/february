const stopOverStartGame = () => {
  saveHighScoreResetPlayer();
  game.scene.stop("gameOver");
  game.scene.stop("mainGame");
  game.scene.start("enterHighScore");
  gameOver.mainGamePaused = false;
  gameOver.mainGameStop = false;
  gameOver.start = false;
  // game.scene.resume("mainGame");
};

const saveHighScoreResetPlayer = () => {
  highScoreObj.lastScore = {pos:-1,score:player.hud.score,name:-1,wave:player.hud.wave,kill:player.hud.kill,miss:player.hud.miss};
  player.hud.power=0;
  player.hud.score=0;
  player.hud.wave=0;
  player.hud.miss=0;
  player.hud.kill=0;
};

function gameOverCreate() {
  player.game.activeScene = 'gameOver';
  this.scene.bringToTop('gameOver');
  gameOver.start = true;
  gameOver.theText = this.add.bitmapText((config.width-320)/2, -32, 'arcade','GAME OVER').setInteractive();

  gameOver.theText.on('pointerdown', pointer => { stopOverStartGame(); });

  // this.input.gamepad.once('down', function(pad, button, index) {
  //   player.inputs.gamepad.activeDevice = pad;
  // }, this);

  this.input.keyboard.on('keyup',function(){
    if(!gameOver.mainGamePaused) return;
    stopOverStartGame();
  },this);
}