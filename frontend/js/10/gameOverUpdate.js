function gameOverUpdate() {
  if(gameOver.theText.y>=config.height/3 && !gameOver.mainGamePaused) {
    this.scene.pause('mainGame');
    gameOver.mainGamePaused = true;
  } else if(gameOver.theText.y<=config.height/2) {
    gameOver.theText.y += 1;
  } else {
    gameOver.mainGameStop = true;
  }

  // if(player.inputs.gamepad.activeDevice||false) {
  //   if(player.inputs.gamepad.activeDevice.A) {
  //     stopOverStartGame();
  //   }
  // }
}