function highScoreEnterNamePreload() {
  this.load.image('block', 'assets/input/block.png');
  this.load.image('rub', 'assets/input/rub.png');
  this.load.image('end', 'assets/input/end.png');
  this.load.bitmapFont('arcade', 'assets/fonts/bitmap/arcade.png', 'assets/fonts/bitmap/arcade.xml');
}