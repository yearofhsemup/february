function highScoreEnterNameCreate() {
  game.scene.stop("mainGame");

  highScoreObj.curMaxX = highScoreObj.chars[0].length-1;
  highScoreObj.curMaxY = highScoreObj.chars.length-1;
  
  highScoreObj.board = loadHighScoreFromLocal();

  let name = '';
  this.input.keyboard.on('keyup', anyKey, this);
  
// 
// todo debug by time
// 
  // this.input.gamepad.once('down', function(pad, button, index) {
  //   player.inputs.gamepad.activeDevice = pad;
  //   console.log('found and activate a gamepad');
  //   console.log('id - '+pad.id);
  //   pad.on('down', function(pad,button,index){
  //     console.log('press i: ');
  //     console.dir({pad,button,index});
  //   },this);
  // }, this);

  // this.input.gamepad.on('down', function(pad, button, index){
  //   console.log('gamepad is somewhere pressed');
  //   console.dir(button);
  // },this);

  const charBoard = this.add.bitmapText(130, 50, 'arcade', 'ABCDEFGHIJKLM\n\nNOPQRSTUVWXYZ\n\n.-<>!@#$%&()').setLetterSpacing(20);
  charBoard.setInteractive();
  let rub = this.add.image(charBoard.x + 586, charBoard.y + 148, 'rub');
  let end = this.add.image(charBoard.x + 638, charBoard.y + 148, 'end');

  highScoreObj.cursorImage = this.add.image(charBoard.x - 10, charBoard.y - 2, 'block').setOrigin(0);

  let scDa = highScoreObj.lastScore;
  let newscore = ("0000000"+scDa.score).substr(-7);
  let newkill = ("000"+scDa.kill).substr(-3);
  let newmiss = ("000"+scDa.miss).substr(-3);
  let staticScoreText = this.add.bitmapText(60, 330, 'arcade', `! ${newscore}  ${scDa.wave} ${newkill} ${newmiss}`).setTint(0x00ff00);

  let legend = this.add.bitmapText(60, 280, 'arcade', '  SCORE   WA Hit Mis   NAME').setTint(0xffff00);

  let rainbowCol = [0xff0000,0xff8200,0xffff00,0x00ff00,0x00bfff,0xffbfff];
  // let colCount = 0;
  for(let lb=0; lb < 3; lb++) {
    let e = highScoreObj.board[lb];
    let newscore = ("0000000"+e.score).substr(-7);
    let newkill = ("000"+e.kill).substr(-3);
    let newmiss = ("000"+e.miss).substr(-3);
    let newname = ("      "+e.name).substr(-5);
    this.add.bitmapText(60, 380+(lb*50), 'arcade', `${e.pos} ${newscore}  ${e.wave} ${newkill} ${newmiss}  ${newname}`).setTint(rainbowCol[lb]);
  }

  highScoreObj.playerNameText = this.add.bitmapText(764, 330, 'arcade', '').setTint(0x00ff00);
  highScoreObj.playerName = '';
  
  this.input.keyboard.on('keyup', function (event) {
    if (event.keyCode === 37) {          //  left
      moveOnHighScore('left');
    } else if (event.keyCode === 39) {   //  right
      moveOnHighScore('right');
    } else if (event.keyCode === 38) {   //  up
      moveOnHighScore('up');
    } else if (event.keyCode === 40) {  //  down
      moveOnHighScore('down');
    }
  });

  charBoard.on('pointermove', function (pointer, x, y) {

    const cx = Phaser.Math.Snap.Floor(x, 52, 0, true);
    const cy = Phaser.Math.Snap.Floor(y, 64, 0, true);
    const char = highScoreObj.chars[cy][cx];

    highScoreObj.cursor.x = cx;
    highScoreObj.cursor.y = cy;

    setVisualCursor();

  }, this);

  charBoard.on('pointerup', function (pointer, x, y) {
    moveOnHighScore('press');
  }, this);
    
}

const moveOnHighScore = (action) => {
  if(action==='left') {
    if (highScoreObj.cursor.x > 0) {
      highScoreObj.cursor.x--;
      highScoreObj.cursorImage.x -= 52;
    }
  } else if(action==='right') {
    if (highScoreObj.cursor.x < highScoreObj.curMaxX) {
      highScoreObj.cursor.x++;
      highScoreObj.cursorImage.x += 52;
    }
  } else if(action==='up') {
    if (highScoreObj.cursor.y > 0) {
      highScoreObj.cursor.y--;
      highScoreObj.cursorImage.y -= 64;
    }
  } else if(action==='down') {
    if (highScoreObj.cursor.y < highScoreObj.curMaxY) {
        highScoreObj.cursor.y++;
        highScoreObj.cursorImage.y += 64;
      }
  } else if(action==='press') {
    if(highScoreObj.cursor.y<=1) {
      let char = highScoreObj.cursor.x + ( highScoreObj.cursor.y * (highScoreObj.curMaxX + 1)) + 65;
      enterName('add',String.fromCharCode(char));
    } else if(highScoreObj.cursor.x===highScoreObj.curMaxX-1 && highScoreObj.cursor.y===highScoreObj.curMaxY) {
      enterName('sub');
    } else if(highScoreObj.cursor.x===highScoreObj.curMaxX && highScoreObj.cursor.y===highScoreObj.curMaxY) {
      enterName('submit');
    }
  }
};

const enterName = (action,str) => {
  if(action==='add' && (str||false) ) {
    if(highScoreObj.playerName.length>=highScoreObj.maxNameChar) return;
    highScoreObj.playerName = highScoreObj.playerName+''+str;
  } else if(action==='sub' && (highScoreObj.playerName.length>=0)) {
    highScoreObj.playerName = highScoreObj.playerName.substr(0, highScoreObj.playerName.length-1);
  } else if(action==='submit' && highScoreObj.playerName.length>=1) {
    console.log("enter the player name: "+highScoreObj.playerName);
    saveNameToHighScore();
  } else {
    return;
  }
  highScoreObj.playerNameText.setText(highScoreObj.playerName);
};

const setVisualCursor = () => {
  highScoreObj.cursorImage.x = 130 - highScoreObj.curMaxX + (highScoreObj.cursor.x*52);
  highScoreObj.cursorImage.y = 50  - highScoreObj.curMaxY + (highScoreObj.cursor.y*64);
};

const anyKey = (e) => {
  let code = event.keyCode;
  let validPress = false;
  if (code === Phaser.Input.Keyboard.KeyCodes.BACKSPACE || code === Phaser.Input.Keyboard.KeyCodes.DELETE) {
      highScoreObj.cursor.x = highScoreObj.curMaxX-1;
      highScoreObj.cursor.y = highScoreObj.curMaxY; 
      validPress = true;
  } else if(code === Phaser.Input.Keyboard.KeyCodes.ENTER) {
      highScoreObj.cursor.x = highScoreObj.curMaxX;
      highScoreObj.cursor.y = highScoreObj.curMaxY; 
      validPress = true;
  } else if(code === Phaser.Input.Keyboard.KeyCodes.SPACE) {
      validPress = true;
  } else if (code >= Phaser.Input.Keyboard.KeyCodes.A && code <= Phaser.Input.Keyboard.KeyCodes.Z) {
      code -= 65;
      highScoreObj.cursor.y = Math.floor(code / (highScoreObj.curMaxX+1));
      highScoreObj.cursor.x = code - (highScoreObj.cursor.y * (highScoreObj.curMaxX+1));
      validPress = true;
  }

  if(validPress) {
    setVisualCursor();
    moveOnHighScore('press');
  }
}

const saveNameToHighScore = () => {
  if(highScoreObj.playerName==="") return;
  highScoreObj.lastScore.name = highScoreObj.playerName;
  highScoreObj.playerName = "";

  let board = loadHighScoreFromLocal();
  board.push(highScoreObj.lastScore);
  board.sort((a,b)=>{return b.score-a.score});
  for(let i=0,il=board.length-1;i<=il;i++) {
    board[i].pos = i + 1;
  }
  console.table(board);
  saveHighScoreToLocal(board);
  highScoreObj.board = board;
  game.scene.stop("enterHighScore");
  game.scene.start("showHighScore");
};

const loadHighScoreFromLocal = () => {
  let rawBoard = localStorage.getItem('highscoreboard');
  let board = [{pos:1,score:642300,name:'deXta',wave:5,kill:423,miss:9}];
  if(rawBoard||false) {
    board = JSON.parse(rawBoard);
  } else {
    board = highScoreObj.board;
  }
  return board;
};

const saveHighScoreToLocal = (board) => {
  localStorage.setItem('highscoreboard', JSON.stringify(board));
};