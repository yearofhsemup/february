const bulletPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Image,
  initialize: function bulletPoolClass(scene) {
    Phaser.GameObjects.Image.call(this, scene, -10, -10, 'bullet');
  },
  fire: function(x,y,sx,sy) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.body.setMass(0.1);
    this.body.setVelocity(sx, sy);
  },
  update: function(time, delta) {
    if(this.x > config.width+10 || this.y < -10 || this.y > config.height+10) {
      this.setVisible(false);
      this.setActive(false);
    }
  }
});

const coinPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  coinList: [1,2,2,3,2,3,2,2,2,3,1,2,3,2,3,2],
  initialize: function coinPoolClass(scene) {
    let nNum = this.coinList.shift();
    let nKey = 'coin' + nNum;
    Phaser.GameObjects.Sprite.call(this, scene, -10, -10, nKey);
    this.anims.load(nKey+"Rot");
    this.anims.play(nKey+"Rot");
    this.setData('type','shoot');
    this.setData('value',nNum);
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.anims.play(this.anims.getCurrentKey(),true,1);
    this.body.setVelocity(-30,15);
    this.setData('score',50);
  },
  update: function(time, delta) {
    if(this.x <= -50 || this.x >= config.width+50 || this.y <= -50 || this.y >= config.height+50) {
      this.setVisible(false);
      this.setActive(false);
    }
  }
});

const powerUpPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function powerUpPoolClass(scene) {
    let nKey = 'powerup' + (parseInt(Math.random()*3)+1);
    Phaser.GameObjects.Sprite.call(this, scene, -10, -10, nKey);
    this.anims.load('powerupRot');
    this.anims.play('powerupRot');
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.body.setMaxSpeed(20);
    this.leafDir = -1;
    this.setData('type','power');
    this.setData('value',1);
    this.setData('score',50);
    this.body.setVelocity(-30,14);
    // this.body.setVelocity(x-50,y-500);
    // console.log(`Bobble speed ${this.body.maxSpeed}`);
  },
  update: function(time, delta) {
    if(this.x <= -50 || this.y <= -50 || this.y >= config.height+50) {
      this.setVisible(false);
      this.setActive(false);
    } 
    // else {
    //   this.setPosition(this.x-(0.2*this.leafDir),this.y);
    //   if(Math.random()>=0.95) this.leafDir *= -1;
    // }
  }
});

// 
// Enemys under here
// 


const fireballPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function fireballPoolClass(scene) {
    Phaser.GameObjects.Sprite.call(this, scene, -10, -10, 'bigFireball');
    this.anims.load("bigFireballFly");
    this.anims.play("bigFireballFly");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.anims.play(this.anims.getCurrentKey(),true,1);
    this.body.setVelocity(-50,-1);
    this.body.setMass(0.1);
    this.setData('score', enemies.state.fireballScore+(player.hud.difficulty*5));
    this.setData('power', enemies.state.fireballPower*player.hud.difficulty);
  },
  update: function(time, delta) {
    if(this.x <= -50 || this.x >= config.width+50 || this.y <= -50 || this.y >= config.height+50) {
      this.setVisible(false);
      this.setActive(false);
    }
  }
});

const asteroidPoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function asteroidPoolClass(scene) {
    let nKey = 'blendAsteroid0' + (parseInt(Math.random()*3)+1);
    Phaser.GameObjects.Sprite.call(this, scene, -10, -10, nKey);
    this.anims.load(nKey+"Fly");
    this.anims.play(nKey+"Fly");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.body.setVelocity(-50,1);
    this.body.setMaxSpeed(60);
    // this.body.setBounce(0,0);
    this.body.setMass(0.5);
    this.setData('score',enemies.state.asteroidScore+(player.hud.difficulty*5));
    this.setData('power',enemies.state.asteroidPower*player.hud.difficulty);
  },
  update: function(time, delta) {
    if(this.x <= -50 || this.y <= -50 || this.y >= config.height+50) {
      this.setVisible(false);
      this.setActive(false);
      addOneMissToScore();
    }
  }
});

const asteroidExplodePoolClass = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function asteroidExplodePoolClass(scene) {
    let nKey = 'blendAsteroid0' + (parseInt(Math.random()*3)+1);
    Phaser.GameObjects.Sprite.call(this, scene, -10, -10, nKey);
    this.anims.load(nKey+"Explode");
    this.anims.play(nKey+"Explode");
  },
  spawn: function(x,y) {
    this.setActive(true);
    this.setVisible(true);
    this.setPosition(x, y);
    this.anims.play(this.anims.getCurrentKey(),true,1);
  },
  update: function(time, delta) {
    if(!this.anims.isPlaying) {
      this.setVisible(false);
      this.setActive(false);
      this.anims.stop(this.anims.getCurrentKey());
    }
  }
});