const addPointsToScore = (pointamount) => {
  player.hud.score += pointamount||0;
  player.hud.textScore.setText("score:"+parseInt(player.hud.score));
};

const addPowerToScore = (pointamount) => {
  player.hud.power += pointamount||0;
  player.hud.textPower.setText("power:"+parseInt(player.hud.power));
};

const addOneMissToScore = () => {
  player.hud.miss += 1;
  player.hud.textMiss.setText("miss:"+parseInt(player.hud.miss));
};

const addOneKillToScore = () => {
  player.hud.kill += 1;
  player.hud.textKill.setText("kill:"+parseInt(player.hud.kill));
};

const addOneWaveToScore = () => {
  player.hud.wave += 1;
  player.hud.textWave.setText("wave:"+parseInt(player.hud.wave));
};

const moveShip = (actionBtn) => {
  if(actionBtn==="left") {
    player.ship.mainSprite.x += -2.5;
    // HUD.fuel -= 0.0015;
  } else if(actionBtn==="right") {
    player.ship.mainSprite.x += 2.5;
    // HUD.fuel -= 0.0015;
  } else if(actionBtn==="down") {
    player.ship.mainSprite.y += 2;
    // HUD.fuel -= 0.001;
  } else if(actionBtn==="up") {
    player.ship.mainSprite.y += -2;
    // HUD.fuel -= 0.001;
  }
};

const updateWaveControll = () => {
  let mWave = Math.floor( ( (player.hud.kill*10) - (player.hud.miss*2) ) / (25 * ( player.hud.wave + 1 ) ) );
  if(mWave>player.hud.wave) {
    addOneWaveToScore();
    enemies.state.nextEnemy = 1000 * ( 8 / (player.hud.wave+1) );
    enemies.state.fireballPower = 5 * (player.hud.wave+1);
    enemies.state.asteroidPower = 1 * (player.hud.wave+1);
  }
}