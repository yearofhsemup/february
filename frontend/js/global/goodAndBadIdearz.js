const FlyingFirePool = new Phaser.Class({
  Extends: Phaser.GameObjects.Sprite,
  initialize: function FlyingFirePool(scene) {
    let tmSp = Phaser.GameObjects.Sprite.call(this, scene, 0, 0, 'fireball', parseInt(Math.random()*4));
    console.dir(tmSp);
    this.speed = Phaser.Math.GetSpeed(1, 100);
  },
  spawn: function(x,y) {
    this.setPosition(x,y);
    this.play('fireballFly');
    this.setActive(true);
    this.setVisible(true);
  },
  update: function(time, delta) {
    this.x -= this.speed * delta;

    if(this.x <= -50) {
      this.setActive(false);
      this.setVisible(false);  
    }
  }
});

class BigFireballPool extends Phaser.GameObjects.Group {
  constructor(game, spriteType, instances, name) {
    super(game.scene);
    this.game = game.scene;
    this.spriteType = spriteType;
    if(instances > 0) {
      let sprite;
      for(let i = 0; i < instances; i++) {
        sprite = this.add(this.game.sprite(330,115, 'bigFireball'));
      }
    }
    return this;
  }

  create(x, y, data) {
    let obj = this.getFirstExists(false);
    if(obj) {
      obj = new this.spriteType(this.game);
      this.add(obj, true);
    }
    return obj.spawn(x, y, data);
  }
}