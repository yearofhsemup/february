const bulletAsteroidCollide = (bullet, asteroid) => {
  if(!bullet.active) return;
  bullet.setActive(false);
  bullet.setVisible(false);
  if(!asteroid.active) return;
  let power = asteroid.getData('power');
  if(power>=player.hud.shootPower||false) {
    asteroid.setData('power',power-player.hud.shootPower);
    return;
  }
  asteroid.setActive(false);
  asteroid.setVisible(false);
  addOneKillToScore();
  let pointz = asteroid.getData('score');
  addPointsToScore(pointz);
  explosionDealer(asteroid.x, asteroid.y);
};

const extraCollectCollide = (ship, extra) => {
  if(!extra.active) return;
  extra.setActive(false);
  extra.setVisible(false);
  let pow = extra.getData('type');
  addPointsToScore(extra.getData('score'));
  if(pow==='power') {
    addPowerToScore(extra.getData('value'));
    extra.setData('value',0);
  } else if(pow==='shoot') {
    player.hud.shootPower = extra.getData('value');
  }
};

const enemyShipCollide = (ship, enemy) => {
  if(!enemy.active||!ship.active) return;
  let enPow = enemy.getData('power')*3;
  enemy.setActive(false);
  enemy.setVisible(false);
  explosionDealer(enemy.x, enemy.y);
  addPointsToScore(enemy.getData('score'));  
  if(enPow>=player.hud.power) {
    player.hud.power = 0;
    ship.setActive(false);
    let fw = ship.width/4;
    let fh = ship.height/4;
    explosionDealer(ship.x, ship.y);
    explosionDealer(ship.x-fw, ship.y-fh);
    explosionDealer(ship.x-fw, ship.y+(fh*3));
    explosionDealer(ship.x+(fw*3), ship.y+(fh*3));
    explosionDealer(ship.x+(fw*3), ship.y+fh);
    ship.setVisible(false);
    game.scene.start("gameOver");
    addPowerToScore(enPow*-1);
  } else {
    addPowerToScore(enPow*-1);
  }
};

const bulletFireballCollide = (bullet, fireball) => {
  if(!bullet.active) return;
  bullet.setActive(false);
  bullet.setVisible(false);
  if(!fireball.active) return;
  fireball.body.setMaxSpeed(50);
  fireball.body.setVelocity(-40,1);
  let power = fireball.getData('power');
  if(power>=player.hud.shootPower||false) {
    fireball.setData('power',power-player.hud.shootPower);
    return;
  }
  fireball.setActive(false);
  fireball.setVisible(false);
  explosionDealer(fireball.x, fireball.y);
};