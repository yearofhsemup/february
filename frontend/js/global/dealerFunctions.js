const enemyDealer = (x,y) => {
  let spawnObj = myObj.asteroidXPool.get();
  if(!spawnObj) { 
    return;
  } else {
    spawnObj.spawn(x, y);
  }
};

const explosionDealer = (x, y) => {
  let exp = myObj.asteroidExplodePool.get();
  if(exp||false) {
    exp.spawn(x, y);    
  }
  bonusDealer(x, y);
};

const bonusDealer = (x,y) => {
  let rollItem = parseInt(Math.random()*10)+1;
  if(rollItem<=8) {
  	if(rollItem<=4) {
      let powerUp = myObj.powerUpPool.get();
      if(powerUp) {
        powerUp.spawn(x, y);
      }
  	} else if(rollItem>=4 && rollItem<=8) {
      //  && rollItem<=7
  		let coin = myObj.coinPool.get();
      if(coin) coin.spawn(x,y);
  	} 
  }
  // else 

  if(rollItem<4) {
		let ball = myObj.fireballPool.get();
		if(ball) {
			ball.setActive(true);
	    ball.setVisible(true);
	    ball.setPosition(x, y);
      ball.body.setMaxSpeed(50);
	    ball.body.setVelocity(-50,1);
		}
	}
}