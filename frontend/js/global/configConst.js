const config = {
    type: Phaser.AUTO,
    width: 960,
    height: 540,
    pixelArt: true,
    backgroundColor: 0x010101,
    input: {
      gamepad: true
    },
    physics: {
      default: 'arcade',
      arcade: {
        debug: false,
        gravity: { y: 1 }
      }
    },
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      // parent: 'phaser-example',
      width: 960,
      height: 540
    },
    // physics: {
    //   default: 'arcade',
    //   arcade: {
    //     gravity: { y: 300 },
    //     debug: false
    //   }
    // },
    scene: [
      { key: 'mainMenu', active: true, preload: mainMenuPreload, create: mainMenuCreate, update: mainMenuUpdate },
      { key: 'options', active: false, preload: optionsPreload, create: optionsCreate, update: optionsUpdate },
      { key: 'showHighScore', active: false,  preload: showHighScorePreload, create: showHighScoreCreate, update: showHighScoreUpdate },
      { key: 'mainGame', active: false,  preload: mainGamePreload, create: mainGameCreate, update: mainGameUpdate },
      { key: 'enterHighScore', active: false, preload: highScoreEnterNamePreload, create: highScoreEnterNameCreate, update: highScoreEnterNameUpdate},
      { key: 'gameOver', active: false, preload: gameOverPreload, create: gameOverCreate, update: gameOverUpdate}
      ],
  // callbacks: {
  //   postBoot: function (game) {
  //     var config = game.config;
  //     var style = game.canvas.style;
  //     style.width = config.width + 'px';
  //     style.height = config.height + 'px';
  //   }
  // }
};

let myObj = {
  bulletPool: null,
  fireballPool:null,
  asteroidXPool:null,
  asteroidExplodePool:null,
  pathEnemy: {
    spritePool: null,
    graphics: null,
    path: null
  },
  powerUpPool: null,
  coinPool: null,
  // cursors: null,
  enemyDealerState: [],
};

let enemies = {
  state: {
    nextEnemy: 1000*2,
    lastEnemy: 0,
    pathEnemyActive: false,
    fireballPower: 3,
    fireballScore: 100,
    asteroidPower: 1,
    asteroidScore: 25,
    pathEnemyPower: 3,
    pathEnemyScore: 50,
  }
};

let player = {
  hud: {
    power: 20,
    difficulty: 1,
    score: 0,
    wave: 0,
    kill:0,
    miss:0,
    shootPower: 1,
    textScore: {},
    textPower: {},
    textKill: {},
    textMiss: {},
    textWave: {},
  },
  ship: {
    mainSprite: null,
    engineSprite: null,
    bulletSprite: null
  },
  game: {
    activeScene: 'highScore',
  },
  inputs: {
    general: {
      autofire: true,
      lastshoot: 0,
      nextshoot: 700,
      notPrell: 0
    },
    pointer: {
      lastX: 0,
      lastY: 0,
      accelerationX: 0,
      accelerationY: 0,
      accelerationMaxX: 1,
      accelerationMaxY: 1,
    },
    cursors: {
      keys: {},
      unprell: 0
    },
    keyboard: {
      cursors: null,
      firebutton: null,
      autofire: null
    },
    gamepad: {
      activeDevice: null,
      lastMove: 0
    }
  },
  bonus: {
    powerFuelDrop: 0.3,
    coinDrop: 0.1,
    powerOverFuel: 0.6,

  },
  // startVars: {
  //   power: 7,
  //   // fuel: 6,
  //   // ships: 2,
  //   shootPower: 1,
  //   nextShoot: 500,
  //   objAsteroids: 10,
  //   objFormEnemy: 10,
  //   objBonusPowFu: 20,
  //   objCoins: 5,
  //   objExplosions: 40,
  //   objMagicExplosions: 30
  // }
};

let highScoreObj = {
  board: [
    {pos:1,score:642300,name:'deXta',wave:5,kill:423,miss:9},
    {pos:2,score:624500,name:'dExTa',wave:5,kill:503,miss:95},
    {pos:3,score:62450,name:'dExTa',wave:5,kill:303,miss:195},
    {pos:4,score:6442,name:'dExTa',wave:5,kill:303,miss:195},
  ],
  gameLogo: null,
  highScoreLogo: null,
  lastScore: {pos:-1,score:64235,name:-1,wave:4,kill:2,miss:3},
  maxNameChar: 5,
  playerName: '',
  playerNameText: null,
  cursor: {x: 0, y:0},
  cursorImage: null,
  chars: [
        [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J','K', 'L', 'M' ],
        [ 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ],
        [ '.', '-', '<', '>', '!', '@', '#', '$', '%', '(', ')' ]
  ],
  curMaxX:0,
  curMaxY:0,
  lastTimePage: 0,
  nextTimePage: 7000,
  pagePos: 0,
  pageTextObj: [],
  entryOnPage: 6,
};

let mainMenu = {
  cursorPointer: null,
  cursorPosition: 0,
  menuItems: [
    {name: 'start', text: null , action: 'mainGame'},
    {name: 'options', text: null , action: 'options'},
    {name: 'about', text: null , action: 'showHighScore'}
  ],
  fontSize: 28,
  colorFill: "#FFF",
  menuX: 0,
  menuY: 0,
  menuSpace: 60
};

let options = {
  optionsItems: [
    {name: 'fullscreen', text: null, action: 'setFullscreen', select: ''},
    {name: 'difficulty', text: null, action: 'setDifficulty', select: 1},
    
  ],
  optionsSpace: 60,
};

let gameOver = {
  start: false,
  theText: null,
  mainGamePaused: false,
  mainGameStop: false,
};

let animConfigs = {
};

animConfigs.fireball = {
  key: 'fireballFly',
  frames: 4,
  frameRate: 6,
  repeat: -1
};

animConfigs.bigfireball = {
  key: 'bigFireballFly',
  frames: 4,
  frameRate: 6,
  repeat: -1
};

animConfigs.blendAsteroid = {
  key: 'blendAsteroid',
  frames: 15,
  frameRate: 12,
  repeat: -1
};

animConfigs.blendAsteroidExplode = {
  key: 'blendAsteroid01Explode',
  frames: 9,
  frameRate: 16,
  hideOnComplete: true,
  repeat: 0
};

animConfigs.coins = {
  key: 'coinRot',
  frames: 10,
  frameRate: 8,
  repeat: -1
};

animConfigs.playerShip = {
  key: 'shipFly',
  frames: 5,
  frameRate: 5,
  repeat: -1
};

// let spriteAnims = {
//   fireball: null,
//   bigfireball: null,
// };



var game = new Phaser.Game(config);