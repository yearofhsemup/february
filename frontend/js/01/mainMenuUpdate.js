function mainMenuUpdate(time) {
  if(player.inputs.general.notPrell>time-200) return;
  player.inputs.general.notPrell=time;
  // if(player.inputs.keyboard.cursors.down.isDown) moveMenuCursor("down");
  // if(player.inputs.keyboard.cursors.up.isDown) moveMenuCursor("up");

  // if(player.inputs.gamepad.activeDevice||false) {
  //   player.inputs.gamepad.lastMove=time;
  //   if(player.inputs.gamepad.activeDevice.up) {
  //     moveMenuCursor("up");
  //   } else if(player.inputs.gamepad.activeDevice.down) {
  //     moveMenuCursor("down");
  //   }
  //   if(player.inputs.gamepad.activeDevice.A) {
  //     menuSelectAction();
  //   }
  // }
}

const moveMenuCursor = (dir) => {
  mainMenu.cursorPosition += (dir==='up')? -1 : 1;
  if(mainMenu.cursorPosition>=mainMenu.menuItems.length) {
    mainMenu.cursorPosition = 0;    
  } else if(mainMenu.cursorPosition<0) {
    mainMenu.cursorPosition = mainMenu.menuItems.length-1;
  }
  let mX = mainMenu.menuX-(mainMenu.menuSpace);
  let mY = (mainMenu.menuY+(mainMenu.fontSize/2))+(mainMenu.cursorPosition*(mainMenu.menuSpace));
  mainMenu.cursorPointer.setPosition(mX , mY);
};

const moveMenuCursorUP = () => {
  moveMenuCursor('up');
};

const moveMenuCursorDOWN = () => {
  moveMenuCursor('down');
};

const menuSelectAction = () => {
  console.log(`do menu action ${mainMenu.cursorPosition} -- ${mainMenu.menuItems[mainMenu.cursorPosition].action}`);
  game.scene.stop('mainMenu');
  game.scene.start(mainMenu.menuItems[mainMenu.cursorPosition].action);
};