function mainMenuPreload() {
  this.load.spritesheet('bigFireball', 'assets/sprites/Jan_big-Fireball.png', { frameWidth: 32, frameHeight: 16 });
  this.load.bitmapFont('arcade', 'assets/fonts/bitmap/arcade.png', 'assets/fonts/bitmap/arcade.xml');
  this.load.image('logo', 'assets/images/feb_pixel_logo_02.png');
}