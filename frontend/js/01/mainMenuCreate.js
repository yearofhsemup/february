function mainMenuCreate() {
  highScoreObj.gameLogo = this.add.image(config.width/2,80, 'logo').setInteractive();
  highScoreObj.gameLogo.once('pointerdown', function(){ 
    game.scene.stop('mainMenu');
    game.scene.start('mainGame');
  });

  let count = 0;
  let menuLen = mainMenu.menuItems.length;
  mainMenu.menuY = config.height/3;
  mainMenu.menuX = config.width/3;
  for(let ni in mainMenu.menuItems) {
    mainMenu.menuItems[ni].text = this.add.bitmapText(mainMenu.menuX, mainMenu.menuY+(count*mainMenu.menuSpace), 'arcade', ''+mainMenu.menuItems[ni].name).setInteractive();
    mainMenu.menuItems[ni].text.on('pointerdown', pointer => { this.scene.start(mainMenu.menuItems[ni].action); });

    count++;
  }

  animConfigs.bigfireball.frames = this.anims.generateFrameNumbers('bigFireball',{ start:0, end: 3});
  this.anims.create(animConfigs.bigfireball);

  mainMenu.cursorPointer = this.add.sprite(mainMenu.menuX-(mainMenu.menuSpace) , mainMenu.menuY+(mainMenu.fontSize/2) , 'bigFireball').setInteractive();
  mainMenu.cursorPointer.flipX = true;
  mainMenu.cursorPointer.setScale(2);
  mainMenu.cursorPointer.anims.load('bigFireballFly');
  mainMenu.cursorPointer.anims.play('bigFireballFly');
  
  inputSetup(this);
}

const inputSetup = (that) => {
  player.inputs.keyboard.cursors = that.input.keyboard.createCursorKeys();
  
  that.input.gamepad.once('down', function(pad, button, index) {
    player.inputs.gamepad.activeDevice = pad;
    console.log("get a gamepad");
  }, that);

  that.input.keyboard.on('keyup_UP', moveMenuCursorUP, that);
  that.input.keyboard.on('keyup_DOWN', moveMenuCursorDOWN, that);
  that.input.keyboard.on('keyup_ENTER', menuSelectAction, that);


  that.input.on('pointermove', pointer => {
    if(!that.scene.isActive('gameOver')) {
      // player.ship.mainSprite.x += pointer.x - player.inputs.pointer.lastX;
      let ny = pointer.y;
      let ly = player.inputs.pointer.lastY;
      let diff = (ny-ly>0)? (ny-ly) : (ly-ny);
      if(diff<mainMenu.fontSize+mainMenu.menuSpace) return;
      player.inputs.pointer.lastY = pointer.y;
      if(ly-ny>0) {
        moveMenuCursor('up');
      } else {
        moveMenuCursor('down');
      }
    }
  });
  

};