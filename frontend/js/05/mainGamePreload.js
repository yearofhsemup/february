function mainGamePreload() {
  this.load.spritesheet('fireball', 'assets/sprites/Jan_Fireball.png', { frameWidth: 16, frameHeight: 8 });
  this.load.spritesheet('bigFireball', 'assets/sprites/Jan_big-Fireball.png', { frameWidth: 32, frameHeight: 16 });

  this.load.spritesheet('blendAsteroid01', 'assets/sprites/Jan_blend_Asteroid_01.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid02', 'assets/sprites/Jan_blend_Asteroid_02.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid05', 'assets/sprites/Jan_blend_Asteroid_03.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid03', 'assets/sprites/Jan_blend_green-Asteroid_01.png', { frameWidth: 32, frameHeight: 32 });
  this.load.spritesheet('blendAsteroid04', 'assets/sprites/Jan_blend_green-Asteroid_02.png', { frameWidth: 32, frameHeight: 32 });

  this.load.spritesheet('ship', 'assets/sprites/Jan_newSmall-PlayerShip.png', { frameWidth: 48, frameHeight: 24 });
  this.load.image('bullet', 'assets/sprites/Feb_oneBullet.png');

  this.load.spritesheet('powerup', 'assets/sprites/Feb_PowerBobble.png', { frameWidth: 16, frameHeight: 16 });
  this.load.spritesheet('coin', 'assets/sprites/Jan_Coin_one23.png', { frameWidth: 16, frameHeight: 16 });
  this.load.bitmapFont('arcade', 'assets/fonts/bitmap/arcade.png', 'assets/fonts/bitmap/arcade.xml');
}