function mainGameCreate() {
  // inputs

  player.inputs.cursors.keys = this.input.keyboard.createCursorKeys();
  player.inputs.keyboard.firebutton = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.X);
  this.input.keyboard.on('keyup', event => {
    if(event.keyCode === 65) { // press a
      player.inputs.general.autofire = (player.inputs.general.autofire||false)? false : true;
    } else if(event.keyCode === 27) {// press esc
      game.scene.start("gameOver");
    } else if(event.keyCode === Phaser.Input.Keyboard.KeyCodes.SPACE) {
      if(game.scene.isActive('mainGame')) {
        game.scene.pause('mainGame');
      } else {
        game.scene.resume('mainGame');
      }
    }
  });

  this.input.on('pointermove', pointer => {
    if(pointer.isDown && !this.scene.isActive('gameOver')) {
      vx = pointer.x - player.inputs.pointer.lastX;
      vy = pointer.y - player.inputs.pointer.lastY;
      player.inputs.pointer.lastX = pointer.x;
      player.inputs.pointer.lastY = pointer.y;
      if(vx>.2) {
        moveShip('right');
      } else if(vx<-.2) {
        moveShip('left');
      }
      if(vy>.2) {
        moveShip('down');
      } else if(vy<-.2) {
        moveShip('up');
      }
    }
  });

  this.input.on('pointerdown', pointer => {
    player.inputs.pointer.lastX = pointer.x;
    player.inputs.pointer.lastY = pointer.y;
  });

  // this.input.gamepad.once('down', function(pad, button, index) {
  //   player.inputs.gamepad.activeDevice = pad;
  // }, this);

  // end inputs

  player.hud.textPower = this.add.bitmapText(16, 16, 'arcade', 'power:'+player.hud.power);
  player.hud.textScore = this.add.bitmapText(16+(24*13), 16, 'arcade', 'score:'+player.hud.score);
  
  player.hud.textMiss = this.add.bitmapText(16, config.height-42, 'arcade', 'miss:'+player.hud.miss);
  player.hud.textKill = this.add.bitmapText(16+(24*12), config.height-42, 'arcade', 'kill:'+player.hud.kill);
  player.hud.textWave = this.add.bitmapText(16+(24*24), config.height-42, 'arcade', 'wave:'+player.hud.wave);
  // player.hud.textFuel = this.add.text(16, 48, 'fuel  : '+player.hud.fuel, { fontSize: '16px', fill: '#FFF' });

  animConfigs.fireball.frames = this.anims.generateFrameNumbers('fireball',{ start:0, end: 3});
  this.anims.create(animConfigs.fireball);

  animConfigs.bigfireball.frames = this.anims.generateFrameNumbers('bigFireball',{ start:0, end: 3});
  this.anims.create(animConfigs.bigfireball);

  asteroidAnimCreate(this,'blendAsteroid01','Fly',0,15);
  asteroidAnimCreate(this,'blendAsteroid01','Explode',16,27);

  asteroidAnimCreate(this,'blendAsteroid02','Fly',0,15);
  asteroidAnimCreate(this,'blendAsteroid02','Explode',16,25);

  asteroidAnimCreate(this,'blendAsteroid03','Fly',0,15);
  asteroidAnimCreate(this,'blendAsteroid03','Explode',16,25);

  asteroidAnimCreate(this,'blendAsteroid04','Fly',0,15);
  asteroidAnimCreate(this,'blendAsteroid04','Explode',16,25);

  asteroidAnimCreate(this,'blendAsteroid05','Fly',0,15);

  animConfigs.fireball.frames = this.anims.generateFrameNumbers('powerup',{ start:0, end: 9});
  animConfigs.fireball.key = 'powerupRot';
  this.anims.create(animConfigs.fireball);

  animConfigs.coins.frames = this.anims.generateFrameNumbers('coin', { start:0, end:9});
  animConfigs.coins.key = 'coin1Rot';
  this.anims.create(animConfigs.coins);
  
  animConfigs.coins.frames = this.anims.generateFrameNumbers('coin', { start:10, end:19});
  animConfigs.coins.key = 'coin2Rot';
  this.anims.create(animConfigs.coins);

  animConfigs.coins.frames = this.anims.generateFrameNumbers('coin', { start:20, end:29});
  animConfigs.coins.key = 'coin3Rot';
  this.anims.create(animConfigs.coins);

  animConfigs.playerShip.frames = this.anims.generateFrameNumbers('ship',{ start:0, end: 4});
  this.anims.create(animConfigs.playerShip);

  myObj.bulletPool = this.physics.add.group({
    classType: bulletPoolClass,
    maxSize: 50,
    runChildUpdate: true
  });

  myObj.asteroidXPool = this.physics.add.group({
    classType: asteroidPoolClass,
    maxSize: 30,
    runChildUpdate: true
  });

  myObj.asteroidExplodePool = this.add.group({
    classType: asteroidExplodePoolClass,
    maxSize: 30,
    runChildUpdate: true
  });

  this.physics.add.collider(myObj.bulletPool, myObj.asteroidXPool, bulletAsteroidCollide, null, this);

  myObj.fireballPool = this.physics.add.group({
    classType: fireballPoolClass,
    maxSize: 20,
    runChildUpdate: true
  });
  // myObj.fireballPool.setVelocity(-50,-100);

  this.physics.add.collider(myObj.bulletPool, myObj.fireballPool, bulletFireballCollide, null, this);

  myObj.powerUpPool = this.physics.add.group({
    classType: powerUpPoolClass,
    maxSize: 10,
    runChildUpdate: true
  });

  myObj.coinPool = this.physics.add.group({
    classType: coinPoolClass,
    maxSize: 10,
    runChildUpdate: true
  });
  // myObj.powerUpPool.setVelocityY(100, 1);

  // myObj.pathEnemy.graphics = this.add.graphics();
  // myObj.pathEnemy.path = new Phaser.Curves.Path(config.width+50, config.height/2);
  // // myObj.pathEnemy.path.lineTo(0, config.height/2);
  // myObj.pathEnemy.path.cubicBezierTo(-50, config.height/2, (config.width/5)*4 , config.height, (config.width/5)*2 , 0);
  
  // myObj.pathEnemy.spritePool = this.physics.add.group();
  // for(let e = 0; e < 10; e++) {
  //   let enemy = myObj.pathEnemy.spritePool.create(config.width+(50*e)+50,config.height/2, 'blendAsteroid05');
  //   enemy.anims.load('blendAsteroid05Fly');
  //   enemy.anims.play('blendAsteroid05Fly');
  //   enemy.setData('vector', new Phaser.Math.Vector2());
  //   enemy.setData('power', enemies.state.pathEnemyPower);
  //   this.tweens.add({
  //     targets: enemy,
  //     z: e,
  //     ease: 'Linear',
  //     duration: 150000,
  //     repeat: -1,
  //     deley: e * 100
  //   });
  // }

  // this.physics.add.collider(myObj.bulletPool, myObj.pathEnemy.spritePool, bulletAsteroidCollide, null, this);

  // Add Player Sprite HuD etc
  player.ship.mainSprite = this.physics.add.sprite(400,160, 'ship');
  player.ship.mainSprite.setVelocity(0,0);
  player.ship.mainSprite.body.setMaxSpeed(0);
  player.ship.mainSprite.anims.load('shipFly');
  player.ship.mainSprite.anims.play('shipFly');

  this.physics.add.collider(myObj.powerUpPool, player.ship.mainSprite, extraCollectCollide, null, this);
  this.physics.add.collider(myObj.coinPool, player.ship.mainSprite, extraCollectCollide, null, this);
  
  this.physics.add.collider(myObj.asteroidXPool, player.ship.mainSprite, enemyShipCollide, null, this);
  this.physics.add.collider(myObj.fireballPool, player.ship.mainSprite, enemyShipCollide, null, this);
  // this.physics.add.collider(myObj.pathEnemy.spritePool, player.ship.mainSprite, enemyShipCollide, null, this);

  player.ship.mainSprite.setBounce(0.2);
  player.ship.mainSprite.setCollideWorldBounds(true);
}