function mainGameUpdate(time,delta) {
  if( !(gameOver.start) && player.inputs.general.lastshoot<time-player.inputs.general.nextshoot) {
    if(player.inputs.general.autofire || player.inputs.keyboard.firebutton.isDown) { 
        playerFireBullets('fire');
        player.inputs.general.lastshoot = time;
      }

    if(player.inputs.gamepad.activeDevice||false) {
      if(player.inputs.gamepad.activeDevice.A) {
        playerFireBullets('fire');
        player.inputs.general.lastshoot = time;
      }
    }
  }
  if(player.inputs.cursors.keys.left.isDown) {
    moveShip('left');
  } else if(player.inputs.cursors.keys.right.isDown) {
    moveShip('right');
  } if(player.inputs.cursors.keys.up.isDown) {
    moveShip('up');
  } if(player.inputs.cursors.keys.down.isDown) {
    moveShip('down');
  }

  if(enemies.state.lastEnemy<time-enemies.state.nextEnemy) {
    let pos = getSomeRandomCoordinate();
    enemyDealer(pos.x,pos.y);
    enemies.state.lastEnemy = time;
  }
  
  if(enemies.state.pathEnemyActive) {
    let pathEnemyZ = myObj.pathEnemy.spritePool.getChildren();
    for(let e=0, el=pathEnemyZ.length; e < el; e++) {
      if(!pathEnemyZ[e].active) continue;
      let t = pathEnemyZ[e].z;
      let vec = pathEnemyZ[e].getData('vector');
      myObj.pathEnemy.path.getPoint(t, vec);
      pathEnemyZ[e].setPosition(vec.x, vec.y);
      pathEnemyZ[e].setDepth(pathEnemyZ[e].y);
    }
  }
    // myObj.pathEnemy.graphics.clear();
    // myObj.pathEnemy.graphics.lineStyle(2, 0xffffff, 1);

    // myObj.pathEnemy.path.draw(myObj.pathEnemy.graphics);

  updateWaveControll();
}


const playerFireBullets = () => {
  let pX = player.ship.mainSprite.x;
  let pY = player.ship.mainSprite.y;
  let nBullet1 = null;
  let nBullet2 = null;
  let nBullet3 = null;
  let nBullet4 = null;
  let nBullet5 = null;
  if(player.hud.shootPower>=1 && (nBullet1 = myObj.bulletPool.get())) {
    nBullet1.fire(pX,pY,100,0);
  }
  if(player.hud.shootPower>=2 && (nBullet2 = myObj.bulletPool.get())) {
    nBullet2.fire(pX,pY,100,12);
  }
  if(player.hud.shootPower>=2 && (nBullet3 = myObj.bulletPool.get())) {
    nBullet3.fire(pX,pY,100,-12);
  }
  if(player.hud.shootPower>=3 && (nBullet4 = myObj.bulletPool.get())) {
    nBullet4.fire(pX,pY,100,24);
  }
  if(player.hud.shootPower>=3 && (nBullet5 = myObj.bulletPool.get())) {
    nBullet5.fire(pX,pY,100,-24);
  }
};


const getSomeRandomCoordinate = () => {
  if(myObj.enemyDealerState.length===0) {
    const oneH = config.height/10;
    for(let i=0;i<=10;i++) {
      let nX = config.width + 50;
      let nY = ( oneH * i) * Math.random();    
      myObj.enemyDealerState.push({r:(Math.random()*1000*1000),x:nX,y:nY});
    }
    myObj.enemyDealerState.sort( (a,b) => { return a.r - b.r } );
  }
  
  const toRe = myObj.enemyDealerState.shift();
  return {x:toRe.x, y:toRe.y};
};