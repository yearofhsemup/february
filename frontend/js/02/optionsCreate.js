function optionsCreate() {
  highScoreObj.gameLogo = this.add.image(config.width/2,80, 'logo').setInteractive();
  highScoreObj.gameLogo.once('pointerdown', function(){ 
    game.scene.stop('options');
    game.scene.start('mainMenu');
  });

  let count = 0;
  // let optionsLen = options.optionsItems.length;
  options.optionsY = config.height/3;
  options.optionsX = config.width/3;
  for(let ni in options.optionsItems) {
    options.optionsItems[ni].text = this.add.bitmapText(options.optionsX, options.optionsY+(count*options.optionsSpace), 'arcade', ''+options.optionsItems[ni].name+' '+options.optionsItems[ni].select).setInteractive();
    options.optionsItems[ni].text.on('pointerdown', pointer => { optionsAction(options.optionsItems[ni].action); });
    count++;
  }
}


const optionsAction = (action) => {
  console.log(action);
  if(action==='setFullscreen') {
    if (game.scale.isFullscreen) {
      game.scale.stopFullscreen();
    } else {
      game.scale.startFullscreen();
    }
  } else if(action==='setDifficulty') {
    let index = options.optionsItems.map( (e) => { return e.name }).indexOf('difficulty');
    options.optionsItems[index].select++;
    if(options.optionsItems[index].select>9) {
      options.optionsItems[index].select=1;
    }
    options.optionsItems[index].text.setText(options.optionsItems[index].name+' '+options.optionsItems[index].select);
  }
  player.hud.difficulty = options.optionsItems[index].select;
};