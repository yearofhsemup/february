function showHighScoreUpdate(time, delta) {
  if(highScoreObj.lastTimePage+highScoreObj.nextTimePage < time ) {
    nextHighScorePage();
    highScoreObj.lastTimePage=time;
  }
}


const nextHighScorePage = (start) => {
  if(start||false) {
    nPos=start;
  } else {
    nPos=highScoreObj.pagePos+1;
  }
  if(nPos*highScoreObj.entryOnPage>=highScoreObj.board.length) nPos=0;
  highScoreObj.pagePos=nPos;
  // let nPos = ( highScoreObj.entryOnPage * highScoreObj.pagePos ) + highScoreObj.entryOnPage;
  tPos=1;
  ePos=highScoreObj.entryOnPage * highScoreObj.pagePos;
  // highScoreObj.pagePos++;
  // if(highScoreObj.pagePos*highScoreObj.entryOnPage>highScoreObj.board.length) highScoreObj.pagePos = 0;
  for(let p=ePos,pl=ePos+highScoreObj.entryOnPage;p<pl;p++) {
    let e = highScoreObj.board[p];
    if(e||false) {
      let newpos = ("  "+e.pos).substr(-2);
      let newwave = ("  "+e.wave).substr(-2);
      let newscore = ("0000000"+e.score).substr(-7);
      let newkill = ("000"+e.kill).substr(-3);
      let newmiss = ("000"+e.miss).substr(-3);
      let newname = ("      "+e.name).substr(-5);
      highScoreObj.pageTextObj[tPos].setText(`${newpos} ${newscore} ${newwave} ${newkill} ${newmiss}  ${newname}`);
    } else {
      highScoreObj.pageTextObj[tPos].setText('');
    }
    tPos++;
  }
};