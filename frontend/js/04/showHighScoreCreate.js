function showHighScoreCreate() {
  this.scene.stop('mainMenu');
  highScoreObj.gameLogo = this.add.image(config.width/2,80, 'logo').setInteractive();
  highScoreObj.gameLogo.once('pointerdown', function(){ 
    goToNextScene('menu');
  });

  highScoreObj.board = loadHighScoreFromLocal();

  highScoreObj.pageTextObj[0] = this.add.bitmapText(30, 170, 'arcade', 'PO   SCORE WA Hit Mis   NAME').setTint(0xffff00);

  let rainbowCol = [0xff0000,0xff8200,0xffff00,0x00ff00,0x00bfff,0xffbfff];

  for(let lb=0; lb < highScoreObj.entryOnPage; lb++) {
    let e = highScoreObj.board[lb];
//    if(!(e||false)) return;
//    let newpos = ("  "+e.pos).substr(-2);
//    let newwave = ("  "+e.wave).substr(-2);
//    let newscore = ("0000000"+e.score).substr(-7);
//    let newkill = ("000"+e.kill).substr(-3);
//    let newmiss = ("000"+e.miss).substr(-3);
//    let newname = ("      "+e.name).substr(-5);
//    highScoreObj.pageTextObj[lb+1] = this.add.bitmapText(30, 220+(lb*50), 'arcade', `${newpos} ${newscore} ${newwave} ${newkill} ${newmiss}  ${newname}`).setTint(rainbowCol[lb]);
    highScoreObj.pageTextObj[lb+1] = this.add.bitmapText(30, 220+(lb*50), 'arcade', '').setTint(rainbowCol[lb]);
  }


  this.input.keyboard.on('keyup', function(key){
    console.dir(key);
    if(key.code==='Escape'||key.code==='Space'||key.code==='enter') {
      goToNextScene('menu');
    } else if(key.code==='KeyR') {
      goToNextScene('game');
    } else if(key.code==='ArrowRight') {
      nextHighScorePage();
    }
  });
}


const goToNextScene = (where) => {
  if(where==='menu') {
    game.scene.stop('showHighScore');
    game.scene.start('mainMenu');
  } else if(where==='game') {
    game.scene.stop('showHighScore');
    game.scene.start('mainGame');
  }
};